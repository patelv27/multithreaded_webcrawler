REQUIREMENT
python 3.5 and up
Unzip the file assignment1_part3.zip to get the python executable files

COMPILATION AND EXECUTION
Run the main function or main file [main.py]

This will ask user to enter number of threads and file name.
Enter the number of threads : <ENTER NUMBER OF THREADS HERE>
Enter Filename : <ENTER FILENAME HERE>

After entering number of threads and file name, press ENTER to start process.

So, the code will give the outputs every 2 seconds with the final output of total number of links, unique IPs, valid DNS names, number of pages successfully 
crawled, robots check and number of pages with respective status code.


INFORMATION
This project processes the given file and crawls all the URLs provided in file one by one in a single thread.

Assignment1_part2 folder structure:
        - main.py
	- myhtread.py
        - myparser.py
        - myrequest.py
        - serverconnect.py
	- globalParams.py

main.py
	- Accepts two inputs: number of threads and file name
	- create_url_queue() method that adds the URLs from the file into a queue and finds the size of file
	- Implements thread by creating class that extends threading.Thread superclass 
	- Creates shared parameters
	- Creates threads with shared parameters
	- Prints statistics and runtime

myhtread.py
	- Keeps track of active threads
	- Validates URL
	- Finds whether the host and ip address are unique,
		*if unique then parse the URL
		*Otherwise continue with next URL
	- Sends head request to check robots.txt
	- If head request status code = 404 and get request status code = 200, then retrieves the total number of links in that URL  

myparser.py
        - Parses the URL using library and returns host, port, path and query
        - Validated the input URL using library validators

myrequest.py
        - sends head request to the designated IP
        - sends get request to selected IP

serverconnect.py
        - Imports requests and BeautifulSoup libraries
	- Get IP address from hostname
	- Extracts links 
	- Send requests to server 
	- Parse data using BeautifulSoup

globalParams.py
	- This file declares the shared parameters like:
		url_queue = None
		print_queue = None
		success_dns = 0
		robots_check = 0
		unique_ips = set()
		unique_hosts = set()
		self.total_seconds = 1
       		self.status_codes = dict()
